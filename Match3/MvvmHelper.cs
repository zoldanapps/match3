﻿using Match3.Model;
using Match3.View;
using Match3.ViewModel;

namespace Match3
{
    public class MvvmHelper
    {
        public static void StartApplication()
        {
            GameModel gamemodel = new GameModel();
            PagesModel pageModel = new PagesModel();
            GameViewModel gameViewModel = new GameViewModel(gamemodel, pageModel);
            GameView gameView = new GameView(gameViewModel);
            gameView.Show();
            pageModel.SelectPage = Common.Enums.ActivePages.MenuPage;
        }
    }
}
