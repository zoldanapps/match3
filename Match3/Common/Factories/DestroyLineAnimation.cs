﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace Match3.Common.Factories
{
    /// <summary>
    /// Анимация взрыва бонусного элемента Line
    /// </summary>
    public class DestroyLineAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _uiBonesFullCollection;
        private readonly Canvas _gameField;
        private readonly List<Controls.BoneControl> _actionUiBones;
        private bool _isAnimationComplited;

        private readonly Image _cloud;
        private readonly Image _destroyer1;
        private readonly Image _destroyer2;

        public DestroyLineAnimation(List<Controls.BoneControl> actionUiBones, List<Controls.BoneControl> uiBonesFullCollection, Canvas gameField)
        {
            _uiBonesFullCollection = uiBonesFullCollection;
            _gameField = gameField;
            _actionUiBones = actionUiBones;
            _cloud = new Image()
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/Cloud.png"))
            };

            _destroyer1 = new Image()
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/Destroyer.png"))
            };

            _destroyer2 = new Image()
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/Destroyer.png"))
            };

        }

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        public void Init(List<Controls.BoneControl> uiBonesFullCollection, Canvas gameField)
        {
            throw new NotImplementedException();
        }

        public void RunAnimation()
        {
            Controls.BoneControl destroyerBone = _actionUiBones.First((b) => b.Bonus == Bonuses.HorizontalLine || b.Bonus == Bonuses.VerticalLine);
            DestroyLine(destroyerBone);          
        }      

        private void DestroyLine(Controls.BoneControl actionUiBone)
        {
            double xc = actionUiBone.ActualWidth / 2 + 2 ;
            double yc = actionUiBone.ActualHeight / 2 + 2 ;
            var rotateTransform = new RotateTransform(0, xc, yc);
            actionUiBone.RenderTransform = rotateTransform;
            Duration duration = new Duration(TimeSpan.FromMilliseconds(200));
            var rotateAnimation = new DoubleAnimation(360, duration)
            {
                RepeatBehavior = new RepeatBehavior(2)
            };
            rotateAnimation.Completed += (a, e) =>
            {
                actionUiBone.Visibility = Visibility.Collapsed;
                _gameField.Children.Remove(actionUiBone);
                _uiBonesFullCollection.Remove(actionUiBone);
                UnSubscribeOnClick?.Invoke(this, new SubscribeArgs(actionUiBone));

                _cloud.Margin = actionUiBone.Margin;
                _gameField.Children.Add(_cloud);

                DoubleAnimation doubleAnimation = new DoubleAnimation(0, duration);
                // По завершении анимации
                doubleAnimation.Completed += (t, o) =>
                {
                    _gameField.Children.Remove(_cloud);
                    RunDestroyers(actionUiBone);
                };
                _cloud.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);
            };

                rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);
        }

        private void RunDestroyers(Controls.BoneControl actionUiBone)
        {
            const double offset = 20;
            TranslateTransform translateTransform1;
            TranslateTransform translateTransform2;
            Duration duration = new Duration(TimeSpan.FromMilliseconds(250));            
            DoubleAnimation moveAnimation1;
            DoubleAnimation moveAnimation2 = new DoubleAnimation(offset, duration);
            double x0, y0;

            if (actionUiBone.Bonus == Bonuses.HorizontalLine)
            {
                moveAnimation1 = new DoubleAnimation(_gameField.ActualWidth - offset, duration);
                y0 = actionUiBone.Margin.Top + offset;
                translateTransform1 = new TranslateTransform(actionUiBone.Margin.Right, y0);
                translateTransform2 = new TranslateTransform(actionUiBone.Margin.Left, y0);                
            }
            else
            {
                x0 = actionUiBone.Margin.Left + offset;                
                moveAnimation1 = new DoubleAnimation(_gameField.ActualHeight - offset, duration);
                translateTransform1 = new TranslateTransform(x0 , actionUiBone.Margin.Top);
                translateTransform2 = new TranslateTransform(x0, actionUiBone.Margin.Bottom);
            }

            moveAnimation1.Completed += (a, e) =>
            {
                _gameField.Children.Remove(_destroyer1);
                if (_isAnimationComplited)
                {
                    DestroyLine();
                }
                else
                    _isAnimationComplited = true;
            };

            moveAnimation2.Completed += (a, e) =>
            {
                _gameField.Children.Remove(_destroyer2);
                if (_isAnimationComplited)
                {
                    DestroyLine();
                }
                else
                    _isAnimationComplited = true;
            };

            _destroyer1.RenderTransform = translateTransform1;
            _destroyer2.RenderTransform = translateTransform2;         

            _gameField.Children.Add(_destroyer1);
            _gameField.Children.Add(_destroyer2);

            if (actionUiBone.Bonus == Bonuses.HorizontalLine)
            {
                translateTransform1.BeginAnimation(TranslateTransform.XProperty, moveAnimation1);
                translateTransform2.BeginAnimation(TranslateTransform.XProperty, moveAnimation2);
            }
            else
            {
                translateTransform1.BeginAnimation(TranslateTransform.YProperty, moveAnimation1);
                translateTransform2.BeginAnimation(TranslateTransform.YProperty, moveAnimation2);
            }
        }       

        private void DestroyLine()
        {
            IAnimation destroyAnimation = new DestroyBonesAnimation(_actionUiBones, _uiBonesFullCollection, _gameField);            
            destroyAnimation.RunAnimation();
            destroyAnimation.UnSubscribeOnClick += UnSubscribeOnClick;
            destroyAnimation.AnimationComplited += AnimationComplited;            
        }
    }
}

