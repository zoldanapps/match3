﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    /// <summary>
    /// Анимация старта игры
    /// </summary>
    public class StartGameAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _uiBones;
        private readonly Canvas _gameField;

        public StartGameAnimation(List<Controls.BoneControl> uiBones, Canvas gameField)
        {
            _uiBones = uiBones;
            _gameField = gameField;
        }

        public event EventHandler AnimationComplited;
        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;      

        public void RunAnimation()
        {
            _gameField.Children.Clear();
            _gameField.Opacity = 0;
            var translateTransform = new TranslateTransform(0, 0);
            foreach (Controls.BoneControl uiBone in _uiBones)
            {
                _gameField.Children.Add(uiBone);                
                SubscribeOnClick?.Invoke(this, new SubscribeArgs(uiBone));
            }
            
            Duration duration = new Duration(TimeSpan.FromMilliseconds(300));
            DoubleAnimation doubleAnimation = new DoubleAnimation(1, duration);
            doubleAnimation.Completed += DoubleAnimation_Completed;
            _gameField.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);
            
        }
        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            AnimationComplited?.Invoke(this, null);
        }        
    }
}
