﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace Match3.Common.Factories
{
   public abstract class AnimationFactory
    {
        protected Canvas _gameField;
        protected List<Controls.BoneControl> _uiBonesFullCollection;
        protected List<Bone> _bones;

        /// <summary>
        /// Создание фабрики
        /// </summary>
        /// <param name="bones">Коллекция фишек над которые будут участвовать в анимации</param>
        public AnimationFactory(List<Bone> bones)
        {
            _bones = bones;
        }      

        /// <summary>
        /// Инициализация фабрики
        /// </summary>
        /// <param name="gameField">Игровое поле</param>
        /// <param name="uiBonesFullCollection">Полная колекция фишек</param>
        public void Init(Canvas gameField, List<Controls.BoneControl> uiBonesFullCollection)
        {
            _gameField = gameField;
            _uiBonesFullCollection = uiBonesFullCollection;         
        }
        public abstract IAnimation Create();

        protected  Controls.BoneControl GetUiBoneById(int id)
        {
            return _uiBonesFullCollection.FirstOrDefault((c) => c.Id == id);
        }

        protected List<Controls.BoneControl> GetUiBonesFromBonesList(List<Bone> bones)
        {
            List<Controls.BoneControl> result = new List<Controls.BoneControl>();
            foreach (Bone bone in bones)
                result.Add(GetUiBoneById(bone.Id));
            return result;
        }
    }
}
