﻿using Match3.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    /// <summary>
    /// Анимация сдвига фишек вниз
    /// </summary>
    public class ShiftDownAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _actionUiBones;
        private readonly int _shift;
        private readonly Canvas _gameField;

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        public ShiftDownAnimation(List<Controls.BoneControl> actionUiBones, Canvas gameField, int shift)
        {
            _actionUiBones = actionUiBones;
            _shift = shift;
            _gameField = gameField;
        }

        public void RunAnimation()
        {
            // Отдельная панель для групповых действий над элементами
            Canvas tempCanvas = new Canvas()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = _gameField.ActualWidth,
                Height = _gameField.ActualHeight
            };
            // добавляем в основную панель
            _gameField.Children.Add(tempCanvas);

            foreach (Controls.BoneControl uiBone in _actionUiBones)
            {
                // Удаляем элементы из основной панели
                _gameField.Children.Remove(uiBone);
                // Добавляем в груповую панель
                tempCanvas.Children.Add(uiBone);
            }

            var translateTransform = new TranslateTransform(0, 0);
            Duration duration = new Duration(TimeSpan.FromMilliseconds(300));
            tempCanvas.RenderTransform = translateTransform;
            var moveAnimation = new DoubleAnimation(_shift * BoneControlHelper.ControlHeight, duration);
            moveAnimation.Completed += (a, e) =>
            {

                _gameField.Children.Remove(tempCanvas);
                tempCanvas.Children.Clear();
                foreach (Controls.BoneControl uiBone in _actionUiBones)
                {
                    uiBone.Margin = BoneControlHelper.Shift(uiBone.Margin, _shift);
                    // Возвращаем элементы обратно
                    _gameField.Children.Add(uiBone);
                }

                AnimationComplited?.Invoke(this, null);
            };
            translateTransform.BeginAnimation(TranslateTransform.YProperty, moveAnimation);
        }


    }
}
