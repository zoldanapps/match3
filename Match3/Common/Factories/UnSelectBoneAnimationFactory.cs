﻿using System;
using System.Collections.Generic;


namespace Match3.Common.Factories
{
    public class UnSelectBoneAnimationFactory : AnimationFactory
    {
        /// <summary>
        /// Фабрика создания анимации отмены выделения фишки
        /// </summary>
        /// <param name="bones">Коллекция элементов над участвующие в анимации</param>
        /// <remarks>В данном случае bones должен содержать один элемент</remarks>
        public UnSelectBoneAnimationFactory(List<Bone> bones)
            : base(bones)
        {

        }
        public override IAnimation Create()
        {
            Controls.BoneControl uiBone = GetUiBoneById(_bones[0].Id);
            IAnimation animation = new UnSelectBoneAnimation(uiBone);            
            return animation;
        }
    }
}
