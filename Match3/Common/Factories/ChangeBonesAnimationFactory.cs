﻿using System.Collections.Generic;


namespace Match3.Common.Factories
{
    public class ChangeBonesAnimationFactory : AnimationFactory
    {

        public ChangeBonesAnimationFactory(List<Bone> bones)
           : base(bones)
        {
        }
        public override IAnimation Create()
        {
            Controls.BoneControl actionUiBone1 = GetUiBoneById(_bones[0].Id);
            Controls.BoneControl actionUiBone2 = GetUiBoneById(_bones[1].Id);
            IAnimation animation = new ChangeBonesAnimation(actionUiBone1, actionUiBone2);            
            return animation;
        }
    }
}
