﻿using System;

namespace Match3.Common.Factories
{
    public class SubscribeArgs: EventArgs
    {
        public Controls.BoneControl UiBone { get; set; }
        public SubscribeArgs(Controls.BoneControl uiBone)
        {
            UiBone = uiBone;
        }
    }
}
