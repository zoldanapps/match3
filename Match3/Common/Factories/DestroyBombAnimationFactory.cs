﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3.Common.Factories
{
    class DestroyBombAnimationFactory : AnimationFactory
    {
        public DestroyBombAnimationFactory(List<Bone> bones)
  : base(bones)
        {
           
        }

        public override IAnimation Create()
        {
            List<Controls.BoneControl> actionUiBones = GetUiBonesFromBonesList(_bones);
            
            IAnimation animation = new DestroyBombAnimation( actionUiBones, _uiBonesFullCollection, _gameField);
            return animation;
        }
    }
}
