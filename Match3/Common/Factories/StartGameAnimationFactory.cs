﻿using Match3.Common.Helpers;
using System.Collections.Generic;

namespace Match3.Common.Factories
{
    public class StartGameAnimationFactory : AnimationFactory
    {
        public StartGameAnimationFactory(List<Bone> bones)
            :base(bones)
        {
        }

        public override IAnimation Create()
        {            
            
            _uiBonesFullCollection.AddRange(BoneControlHelper.MapToBonesControl(_bones));

            IAnimation animation = new StartGameAnimation(_uiBonesFullCollection, _gameField);            
            return animation;
        }
    }
}
