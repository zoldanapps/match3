﻿using Common.Enums;
using System.Collections.Generic;
using System.Linq;


namespace Match3.Common.Factories
{
    public class DestroyBonusAnimationFactory : AnimationFactory
    {

        private readonly Bone _destroyerBone;

        public DestroyBonusAnimationFactory(List<Bone> bones)
     : base(bones)
        {
            _destroyerBone = bones.First((b) => b.Bonus != Bonuses.None);

        }

        public override IAnimation Create()
        {
            List<Controls.BoneControl> actionUiBones = GetUiBonesFromBonesList(_bones);
            IAnimation animation;
            
            if (_destroyerBone.Bonus == Bonuses.HorizontalLine || _destroyerBone.Bonus == Bonuses.VerticalLine)
                animation = new DestroyLineAnimation(actionUiBones, _uiBonesFullCollection, _gameField);
            else
                animation = new DestroyBombAnimation(actionUiBones, _uiBonesFullCollection, _gameField);
            return animation;
        }
    }
}
