﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Media;

namespace Match3.Common.Factories
{
    public class UnSelectBoneAnimation : IAnimation
    {
        private readonly Controls.BoneControl _actionUiBone;

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;       

        /// <summary>
        /// Создание анимации отмены выделения фишки
        /// </summary>
        /// <param name="actionUiBone">Выделяемый объект</param>
        public UnSelectBoneAnimation(Controls.BoneControl actionUiBone)
        {
            _actionUiBone = actionUiBone;
        }

        public void RunAnimation()
        {
            _actionUiBone.RenderTransform.BeginAnimation(RotateTransform.AngleProperty, null);
        }
    }
}
