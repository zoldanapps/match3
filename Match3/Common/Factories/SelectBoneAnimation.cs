﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    public class SelectBoneAnimation : IAnimation
    {        
        private readonly Controls.BoneControl _actionUiBone;        

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        /// <summary>
        /// Создание анимации выделения фишки
        /// </summary>
        /// <param name="actionUiBone">Выделяемый объект</param>
        public SelectBoneAnimation(Controls.BoneControl actionUiBone)
        {
            _actionUiBone = actionUiBone;
        }       

        /// <summary>
        /// Запуск анимации
        /// </summary>
        public void RunAnimation()
        {            
            double xc = _actionUiBone.ActualWidth / 2;
            double yc = _actionUiBone.ActualHeight / 2;
            var rotateTransform = new RotateTransform(0, xc, yc);
            _actionUiBone.RenderTransform = rotateTransform;
            Duration duration = new Duration(TimeSpan.FromMilliseconds(500));
            var rotateAnimation = new DoubleAnimation(360, duration)
            {
                RepeatBehavior = RepeatBehavior.Forever
            };
            rotateAnimation.Completed += RotateAnimation_Completed;
            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);
        }

        private void RotateAnimation_Completed(object sender, EventArgs e)
        {
            AnimationComplited?.Invoke(this, null);
        }
    }
}
