﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Match3.Common.Factories
{
   public interface IAnimation
    {
        /// <summary>
        /// Уведомление о том что нужно подписать элемент на событие Click
        /// </summary>
        event EventHandler<SubscribeArgs> SubscribeOnClick;

        /// <summary>
        /// Уведомление о том что нужно отписать элемент на событие Click
        /// </summary>
        event EventHandler<SubscribeArgs> UnSubscribeOnClick;

        event EventHandler AnimationComplited;
        
        /// <summary>
        /// Запуск анимации
        /// </summary>
        void RunAnimation();
    }
}
