﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    /// <summary>
    /// Анимация разрушения фишек
    /// </summary>
    public class DestroyBonesAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _actionUiBones;
        private readonly List<Controls.BoneControl> _uiBonesFullCollection;
        private readonly Canvas _gameField;
               

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        public DestroyBonesAnimation(List<Controls.BoneControl> actionUiBones, List<Controls.BoneControl> uiBonesFullCollection, Canvas gameField)
        {
            _actionUiBones = actionUiBones;
            _uiBonesFullCollection = uiBonesFullCollection;
            _gameField = gameField;
        }

        public void RunAnimation()
        {

            // Отдельная панель для групповых действий над элементами
            Canvas tempCanvas = new Canvas()
            {
                VerticalAlignment = VerticalAlignment.Stretch,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Width = _gameField.ActualWidth,
                Height = _gameField.ActualHeight,
            };
            // добавляем в основную панель
            _gameField.Children.Add(tempCanvas);

            foreach (Controls.BoneControl uiBone in _actionUiBones)
            {
                try
                {
                    // Удаляем элементы из основной панели
                    _gameField.Children.Remove(uiBone);
                    // Сообщаем, что нужно отписать элемент                
                    UnSubscribeOnClick?.Invoke(this, new SubscribeArgs(uiBone));
                    // Добавляем в груповую панель
                    tempCanvas.Children.Add(uiBone);
                    // Удаляем из контейнера
                    _uiBonesFullCollection.Remove(uiBone);
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            }
            // Анимация изменения прозрачности временной панели
            Duration duration = new Duration(TimeSpan.FromMilliseconds(200));
            DoubleAnimation doubleAnimation = new DoubleAnimation(0, duration)
            {
                RepeatBehavior = new RepeatBehavior(5)
            };
            // По завершении анимации
            doubleAnimation.Completed += (a, e) => {
                // Удаляем временну панель из основной
                _gameField.Children.Remove(tempCanvas);
                // Разрушаем все содержимое временной панели
                tempCanvas.Children.Clear();
                AnimationComplited?.Invoke(this, null);
            };
            tempCanvas.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);

        }
    }
}
