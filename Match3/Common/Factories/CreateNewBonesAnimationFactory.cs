﻿using Match3.Common.Helpers;
using System.Collections.Generic;

namespace Match3.Common.Factories
{
    public class CreateNewBonesAnimationFactory : AnimationFactory
    {

        public CreateNewBonesAnimationFactory(List<Bone> bones)
       : base(bones)
        {
        }

        public override IAnimation Create()
        {
            List<Controls.BoneControl> actionUiBones = BoneControlHelper.MapToBonesControl(_bones);            

            IAnimation animation = new CreateNewBonesAnimation(actionUiBones, _uiBonesFullCollection, _gameField);            
            return animation;
        }
    }
}
