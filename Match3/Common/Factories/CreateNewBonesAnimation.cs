﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    public class CreateNewBonesAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _actionUiBones;
        private readonly Canvas _gameField;
        private readonly List<Controls.BoneControl> _uiBonesFullCollection;

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        /// <summary>
        /// Создание новой фишки
        /// </summary>
        /// <param name="actionUiBone">Выделяемый объект</param>
        public CreateNewBonesAnimation(List<Controls.BoneControl> actionUiBones, List<Controls.BoneControl> uiBonesFullCollection, Canvas gameField)
        {
            _actionUiBones = actionUiBones;
            _gameField = gameField;
            _uiBonesFullCollection = uiBonesFullCollection;
        }

        public void RunAnimation()
        {

            // Отдельная панель для групповых действий над элементами
            Canvas tempCanvas = new Canvas()
            {
                HorizontalAlignment = HorizontalAlignment.Stretch,
                VerticalAlignment = VerticalAlignment.Stretch,
                Width = _gameField.ActualWidth,
                Height = _gameField.ActualHeight,
            };
            // Заполняем панель элементами анимации
            foreach (Controls.BoneControl uiBone in _actionUiBones)
                tempCanvas.Children.Add(uiBone);

            // добавляем в временную панель в основную панель
            _gameField.Children.Add(tempCanvas);
           
            Duration duration = new Duration(TimeSpan.FromMilliseconds(200));
            DoubleAnimation doubleAnimation = new DoubleAnimation(0, duration)
            {
                RepeatBehavior = new RepeatBehavior(5)
            };
            // По завершении анимации
            doubleAnimation.Completed += (a, e) => {
                // Удаляем временну панель из основной
                _gameField.Children.Remove(tempCanvas);
                // Разрушаем все содержимое временной панели
                tempCanvas.Children.Clear();
                // Заполняем игровую панель новыми элементами
                foreach (Controls.BoneControl uiBone in _actionUiBones)
                {
                    _gameField.Children.Add(uiBone);
                    _uiBonesFullCollection.Add(uiBone);
                    SubscribeOnClick?.Invoke(this, new SubscribeArgs(uiBone));
                }
                AnimationComplited?.Invoke(this, null);
            };
            tempCanvas.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);
        }
    }
}
