﻿using System;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace Match3.Common.Factories
{
    class ChangeBonesAnimation : IAnimation
    {       
        
        private readonly Controls.BoneControl _actionUiBone1;
        private readonly Controls.BoneControl _actionUiBone2;
        private bool _isAnimationComplited;

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;
        
        /// <summary>
        /// Анимация выделения фишек и обмен местами
        /// </summary>
        /// <param name="actionUiBone">Выделяемый объект</param>
        public ChangeBonesAnimation(Controls.BoneControl actionUiBone1, Controls.BoneControl actionUiBone2)
        {
            _actionUiBone1 = actionUiBone1;
            _actionUiBone2 = actionUiBone2;
        }

        public void RunAnimation()
        {           

            var translateTransform1 = new TranslateTransform(0, 0);
            var translateTransform2 = new TranslateTransform(0, 0);
            Duration duration = new Duration(TimeSpan.FromMilliseconds(250));
            double d = 0;
            // Меняем по вертикали
            if (_actionUiBone1.Margin.Top == _actionUiBone2.Margin.Top)
            {
                _actionUiBone1.RenderTransform = translateTransform1;
                d = _actionUiBone1.Margin.Left > _actionUiBone2.Margin.Left ? -_actionUiBone1.ActualWidth : _actionUiBone1.ActualWidth;
                var moveAnimation1 = new DoubleAnimation(d, duration);
                moveAnimation1.Completed += (a, e) =>
                {
                    translateTransform1.BeginAnimation(TranslateTransform.XProperty, null);
                    if (_isAnimationComplited)
                    {
                        ActionComplited();
                    }
                    else
                        _isAnimationComplited = true;
                };
                

                _actionUiBone2.RenderTransform = translateTransform2;
                var moveAnimation2 = new DoubleAnimation(-d, duration);
                moveAnimation2.Completed += (a, e) =>
                {
                    translateTransform2.BeginAnimation(TranslateTransform.XProperty, null);
                    if (_isAnimationComplited)
                    {
                        ActionComplited();
                    }
                    else
                        _isAnimationComplited = true;
                };


                translateTransform1.BeginAnimation(TranslateTransform.XProperty, moveAnimation1);
                translateTransform2.BeginAnimation(TranslateTransform.XProperty, moveAnimation2);
            }

            // Меняем по горизонтали
            if (_actionUiBone1.Margin.Left == _actionUiBone2.Margin.Left)
            {
                _actionUiBone1.RenderTransform = translateTransform1;
                 d = _actionUiBone1.Margin.Top > _actionUiBone2.Margin.Top ? -_actionUiBone1.ActualHeight : _actionUiBone1.ActualHeight;
                var moveAnimation1 = new DoubleAnimation(d, duration);                                
                moveAnimation1.Completed += (a, e) =>
                {
                    translateTransform1.BeginAnimation(TranslateTransform.YProperty, null);
                    if (_isAnimationComplited)
                    {

                        ActionComplited();
                    }
                    else
                        _isAnimationComplited = true;
                };

                _actionUiBone2.RenderTransform = translateTransform2;
                var moveAnimation2 = new DoubleAnimation(-d, duration);                
                moveAnimation2.Completed += (a, e) =>
                {
                    translateTransform2.BeginAnimation(TranslateTransform.YProperty, null);
                    moveAnimation2 = null;
                    if (_isAnimationComplited)
                    {
                        ActionComplited();
                    }
                    else
                        _isAnimationComplited = true;
                };               

                 translateTransform1.BeginAnimation(TranslateTransform.YProperty, moveAnimation1);
                 translateTransform2.BeginAnimation(TranslateTransform.YProperty, moveAnimation2);
            }
        }

        private void ActionComplited()
        {
            Thickness temp = _actionUiBone1.Margin;
            _actionUiBone1.Margin = _actionUiBone2.Margin;
            _actionUiBone2.Margin = temp;
            AnimationComplited?.Invoke(this, null);
        }
     
    }
}

