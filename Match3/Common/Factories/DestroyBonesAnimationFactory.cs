﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3.Common.Factories
{
    public class DestroyBonesAnimationFactory : AnimationFactory
    {

        public DestroyBonesAnimationFactory(List<Bone> bones)
         : base(bones)
        {
        }
        public override IAnimation Create()
        {
            List<Controls.BoneControl> actionsUiBones = GetUiBonesFromBonesList(_bones);
            
            IAnimation animation = new DestroyBonesAnimation(actionsUiBones, _uiBonesFullCollection, _gameField);
            
            return animation;
        }
    }
}
