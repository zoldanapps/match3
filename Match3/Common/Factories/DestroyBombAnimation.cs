﻿using Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;

namespace Match3.Common.Factories
{
    /// <summary>
    /// Анимация взрыва бонуса Bomb
    /// </summary>
    public class DestroyBombAnimation : IAnimation
    {
        private readonly List<Controls.BoneControl> _uiBonesFullCollection;

        private readonly Canvas _gameField;

        private readonly List<Controls.BoneControl> _actionUiBones;

        private readonly Image _cloud;

        public event EventHandler<SubscribeArgs> SubscribeOnClick;
        public event EventHandler<SubscribeArgs> UnSubscribeOnClick;
        public event EventHandler AnimationComplited;

        
        public DestroyBombAnimation(List<Controls.BoneControl> actionUiBones, List<Controls.BoneControl> uiBonesFullCollection, Canvas gameField)
        {
            _uiBonesFullCollection = uiBonesFullCollection;
            _gameField = gameField;
            _actionUiBones = actionUiBones;
            _cloud = new Image()
            {
                Source = new BitmapImage(new Uri("pack://application:,,,/Images/Cloud.png"))
            };
        }       

        public void RunAnimation()
        {
            Controls.BoneControl destroyerBone = _actionUiBones.First((b) => b.Bonus == Bonuses.Bomb);
            DestroyBomb(destroyerBone);
        }

        private void DestroyBomb(Controls.BoneControl actionUiBone)
        {
            double xc = actionUiBone.ActualWidth / 2 + 2;
            double yc = actionUiBone.ActualHeight / 2 + 2;
            var rotateTransform = new RotateTransform(0, xc, yc);
            actionUiBone.RenderTransform = rotateTransform;
            Duration duration = new Duration(TimeSpan.FromMilliseconds(200));
            var rotateAnimation = new DoubleAnimation(360, duration)
            {
                RepeatBehavior = new RepeatBehavior(2)
            };
            rotateAnimation.Completed += (a, e) =>
            {
                actionUiBone.Visibility = Visibility.Collapsed;
                _gameField.Children.Remove(actionUiBone);
                _uiBonesFullCollection.Remove(actionUiBone);
                UnSubscribeOnClick?.Invoke(this, new SubscribeArgs(actionUiBone));

                _cloud.Margin = actionUiBone.Margin;
                _gameField.Children.Add(_cloud);

                DoubleAnimation doubleAnimation = new DoubleAnimation(0, duration);
                // По завершении анимации
                doubleAnimation.Completed += (t, o) =>
                {
                    _gameField.Children.Remove(_cloud);
                    DestroyGroupe();
                };
                _cloud.BeginAnimation(UIElement.OpacityProperty, doubleAnimation);
            };

            rotateTransform.BeginAnimation(RotateTransform.AngleProperty, rotateAnimation);
        }

        private void DestroyGroupe()
        {
            IAnimation destroyAnimation = new DestroyBonesAnimation(_actionUiBones, _uiBonesFullCollection, _gameField);            
            destroyAnimation.RunAnimation();
            destroyAnimation.UnSubscribeOnClick += UnSubscribeOnClick;
            destroyAnimation.AnimationComplited += AnimationComplited;            
        }
    }
}
