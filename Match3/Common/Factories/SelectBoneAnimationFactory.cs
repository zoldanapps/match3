﻿
using System.Collections.Generic;

namespace Match3.Common.Factories
{
    public class SelectBoneAnimationFactory : AnimationFactory
    {

        /// <summary>
        /// Фабрика создания анимации выделения фишки
        /// </summary>
        /// <param name="bones">Коллекция элементов над участвующие в анимации</param>
        /// <remarks>В данном случае bones должен содержать один элемент</remarks>
        public SelectBoneAnimationFactory(List<Bone> bones)
            : base(bones)
        {

        }

        public override IAnimation Create()
        {
            Controls.BoneControl uiBone = GetUiBoneById(_bones[0].Id);
            IAnimation animation = new SelectBoneAnimation(uiBone);

            
            return animation;
        }
    }
}
