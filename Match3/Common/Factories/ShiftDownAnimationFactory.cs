﻿using System.Collections.Generic;


namespace Match3.Common.Factories
{
    public class ShiftDownAnimationFactory : AnimationFactory
    {
        private readonly int _shift;

        public ShiftDownAnimationFactory(List<Bone> bones, int shift)
     : base(bones)
        {
            _shift = shift;
        }
        public override IAnimation Create()
        {
            List<Controls.BoneControl> actionUiBones = GetUiBonesFromBonesList(_bones);

            IAnimation animation = new ShiftDownAnimation(actionUiBones, _gameField, _shift);
            return animation;
        }
    }
}
