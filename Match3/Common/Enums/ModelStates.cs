﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3.Common.Enums
{
    /// <summary>
    /// Состояния модели
    /// </summary>
    public enum ModelStates
    {
        None,
        /// <summary>
        /// Начало игры
        /// </summary>
        NewGame,
        /// <summary>
        /// Ждет выделения фишек
        /// </summary>
        SelectBone,
        /// <summary>
        /// Поменять фишки местами
        /// </summary>
        ChangeBones,
        /// <summary>
        /// Попытка обмена фишками
        /// </summary>
        TryChangeBones,        
        /// <summary>
        /// Удалить выпавшие фишки
        /// </summary>
        DestroyBones,
        /// <summary>
        /// Удалить бонусы
        /// </summary>
        DestroyBonusBones,
        /// <summary>
        /// СОздать новую фишку с бонусом
        /// </summary>
        CreateBonusBones,
        /// <summary>
        /// Сдвинуть фишки вниз
        /// </summary>
        ShiftBones,
        /// <summary>
        /// Проанализировать матрицу, восстановить пустые места. взорвать случайно 
        /// выстроившиеся фишки
        /// </summary>
        RestoreMatrix
    }
}
