﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Match3.Common.Enums
{
    /// <summary>
    /// Активная в UI в данные момент страница
    /// </summary>
    public enum ActivePages
    {
        None,
        GamePage,
        MenuPage,
        GameOverPage
    }
}
