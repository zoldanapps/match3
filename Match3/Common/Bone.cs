﻿using Common.Enums;
using System;

namespace Match3.Common
{
    public class Bone : ICloneable
    {
        public BoneColors Color { get; set; }
        public Bonuses Bonus { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Id { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
