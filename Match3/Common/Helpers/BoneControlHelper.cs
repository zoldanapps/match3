﻿using System.Collections.Generic;
using System.Windows;

namespace Match3.Common.Helpers
{
    public static class BoneControlHelper
    {
        public const double ControlHeight = 66;
        public const double ControlWidth = 66;
        /// <summary>
        /// Растояние между фишками
        /// </summary>
        public const double Gap = 10;

        public static Controls.BoneControl MapToBoneControl(Bone bone)
        {
            return new Controls.BoneControl()
            {
                Margin = GetMarginFromPosition(bone.X, bone.Y),
                BoneColor = bone.Color,
                Id = bone.Id,
                Bonus = bone.Bonus,
                Width = ControlWidth,
                Height = ControlHeight
            };            
        }

        public static List<Controls.BoneControl> MapToBonesControl(List<Bone> bones)
        {
            List<Controls.BoneControl> result = new List<Controls.BoneControl>(); 
            foreach (Bone bone in bones)
            {
                result.Add(MapToBoneControl(bone));
            }
            return result;
        }

        public static double GetX(double posX)
        {
            return (posX - 1) * ControlWidth + Gap;
        }
        public static double GetY(double posY)
        {
            return (posY - 1) * ControlHeight + Gap;
        }

        public static Thickness GetMarginFromPosition(double posX, double posY)
        {
            double X = GetX(posX);
            double Y = GetY(posY);
            return new Thickness(X, Y, X + ControlWidth, Y + ControlHeight);
        }

        public static Thickness Shift(Thickness margin, double shift)
        {
            double X = margin.Left;
            double Y = margin.Top + shift * ControlHeight;
            return new Thickness(X, Y, X + ControlWidth, Y + ControlHeight);
        }
    }
}
