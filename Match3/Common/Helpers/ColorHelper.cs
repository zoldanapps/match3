﻿using Common.Enums;
using System;

namespace Match3.Common.Helpers
{
    public static class ColorHelper
    {

        private static readonly Random _rnd = new Random();
        /// <summary>
        /// Находит случайный цвет такой что он не равен originalColor
        /// </summary>
        /// <param name="originalColor"></param>
        /// <returns></returns>
        public static BoneColors FindRandomColor(BoneColors originalColor)
        {
            BoneColors rndColor;
            do
            {
                rndColor = GetRandomColor();
            }
            while (rndColor == originalColor);
            return rndColor;
        }

        /// <summary>
        /// Находит случайный цвет такой что он не равен originalColor1 и originalColor2
        /// </summary>
        /// <param name="originalColor1"></param>
        /// <param name="originalColor2"></param>
        /// <returns></returns>
        public static BoneColors FindRandomColor(BoneColors originalColor1, BoneColors originalColor2)
        {
            BoneColors rndColor;
            do
            {
                rndColor = GetRandomColor();
            }
            while (!(rndColor != originalColor1 && rndColor != originalColor2));
            return rndColor;
        }       

        /// <summary>
        /// Возвращает рандомный цвет
        /// </summary>
        /// <returns></returns>
        public static BoneColors GetRandomColor()
        {
            return (BoneColors)_rnd.Next(0, 5); ;
        }
    }
}
