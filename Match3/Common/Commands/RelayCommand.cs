﻿using System;
using System.Windows.Input;

namespace Match3.Common.Commands
{
    public class RelayCommand : ICommand
    {

        protected Action<object> _commandAction;
        public event EventHandler CanExecuteChanged;

        public RelayCommand()
        {

        }

        public void SetAction(Action<object> commanAction)
        {
            _commandAction = commanAction;
        }

        public virtual bool CanExecute(object parameter)
        {
            return true;
        }

        public virtual void Execute(object parameter)
        {
            _commandAction?.Invoke(parameter);
        }
    }
}
