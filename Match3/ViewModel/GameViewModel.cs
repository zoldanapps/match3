﻿using Match3.Common.Commands;
using Match3.Common.Enums;
using Match3.Common.Factories;
using Match3.Model;
using System.ComponentModel;
using System.Windows;


namespace Match3.ViewModel
{
    public class GameViewModel: MvvmBase
    {
        private readonly GameModel _gameModel;
        private readonly PagesModel _pagesModel;
       
        public GameViewModel(GameModel gameModel, PagesModel pagesModel)
        {
            StartActionCommand = new RelayCommand();
            ActionComplitedCommand = new RelayCommand();
            SelectBoneCommand = new RelayCommand();            
            PlayGameCommand = new RelayCommand();
            

            _gameModel = gameModel;
            _pagesModel = pagesModel;
            _gameModel.NewGameStarted += GameModel_NewGameStarted;
            _gameModel.BoneSelected += GameModel_SelectedBone;
            _gameModel.BonesChenged += GameModel_ChengeddBones;
            _gameModel.SelectReseted += GameModel_SelectReseted;
            _gameModel.BonesDestroyed += GameModel_DestroyedBones;
            _gameModel.NewBonesCreated += GameModel_CreatedNewBones;
            _gameModel.BonusBoneDestroyed += GameModel_DestroyedBonusBone;
            _gameModel.BonesSheeted += GameModel_SheeftBones;            
            _gameModel.GameOver += GameModel_GameOver;
            _gameModel.PropertyChanged += GameModel_PropertyChanged;
            _pagesModel.PropertyChanged += PagesModel_PropertyChanged;


            PlayGameCommand.SetAction((p) => 
            {
                _pagesModel.SelectPage = ActivePages.GamePage;
            });

            SelectBoneCommand.SetAction((bid) =>
            {
                int boneId = (int)bid;
                _gameModel.SelectBone(boneId);
            });

            ActionComplitedCommand.SetAction((e) =>
            {
                _gameModel.UpdateState();
            });
            
        }

        private void PagesModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectPage")
            {                
                if (_pagesModel.SelectPage == ActivePages.GamePage)
                {
                    SelectPage = _pagesModel.SelectPage;
                    OnPropertyChanged(e.PropertyName);
                    _gameModel.StartNewGame();
                    return;
                }  
                if (_pagesModel.SelectPage == ActivePages.GameOverPage)
                {
                    Application.Current.Dispatcher.Invoke(() => {
                        SelectPage = _pagesModel.SelectPage;
                        OnPropertyChanged(e.PropertyName);
                    });
                    return;                    
                }
                SelectPage = _pagesModel.SelectPage;
                OnPropertyChanged(e.PropertyName);

            }
            
        }

        private void GameModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SecondCount")
            {
               Application.Current.Dispatcher?.Invoke(() => { SecondCount = _gameModel.SecondCount.ToString(); });                
            }            
            OnPropertyChanged(e.PropertyName);
        }      

        private void GameModel_GameOver(object sender, ScoreEventArgs e)
        {
            _pagesModel.SelectPage = ActivePages.GameOverPage;
        }       

        private void GameModel_SheeftBones(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new ShiftDownAnimationFactory(e.Bones, e.Depth);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_DestroyedBonusBone(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new DestroyBonusAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_CreatedNewBones(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new CreateNewBonesAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_DestroyedBones(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new DestroyBonesAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_SelectReseted(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new UnSelectBoneAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_ChengeddBones(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new ChangeBonesAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_SelectedBone(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new SelectBoneAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        private void GameModel_NewGameStarted(object sender, ModelActionEventArgs e)
        {
            AnimationFactory animationFactory = new StartGameAnimationFactory(e.Bones);
            StartActionCommand.Execute(animationFactory);
        }

        public string SecondCount { get; set; }
        public string ScoreCount => _gameModel.ScoreCount.ToString();
        public ActivePages SelectPage { get; set; }

        public RelayCommand StartActionCommand { get; set; }
        public RelayCommand ActionComplitedCommand { get; set; }
        public RelayCommand SelectBoneCommand { get; set; }        
        public RelayCommand PlayGameCommand { get; set; }
       

    }
}

