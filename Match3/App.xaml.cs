﻿
using System.Windows;

namespace Match3
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            MvvmHelper.StartApplication();            
        }
    }
}
