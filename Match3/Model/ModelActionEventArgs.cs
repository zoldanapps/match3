﻿using Match3.Common;
using System.Collections.Generic;

namespace Match3.Model
{
    public class ModelActionEventArgs
    {
        public List<Bone> Bones { get; }
        public int Depth { get; set; }
        public ModelActionEventArgs (List<Bone> bones)
        {
            Bones = bones;
        }
    }
}
