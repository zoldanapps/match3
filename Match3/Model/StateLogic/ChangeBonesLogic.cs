﻿using Match3.Common;
using Match3.Model.StateAnalyzers;
using System.Collections.Generic;

namespace Match3.Model.StateLogic
{
    /// <summary>
    /// Реализация логики обмена фишек местами
    /// </summary>
    public class ChangeBonesLogic
    {
        private readonly GameMatrix _gameMatrix;        
        private readonly LinesAnalyzer _linesAnalizer;

        public List<Bone> SelectedBones  => _linesAnalizer.SelectedBones;
        public List<Bone> BonusBones => _linesAnalizer.BonusBones;

        public ChangeBonesLogic(GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
            _linesAnalizer = new LinesAnalyzer(_gameMatrix);
        }

        /// <summary>
        /// Возвращает true, если перемещение соответствует логике игры
        /// </summary>
        /// <param name="firstBone"></param>
        /// <param name="lastBone"></param>
        /// <returns></returns>
        public bool TryChangeBone(Bone firstBone, Bone lastBone)
        {            
            _gameMatrix.ChangeBones(firstBone, lastBone);
            var result = _linesAnalizer.Analyze(firstBone, lastBone);
            if (!result)
                _gameMatrix.ChangeBones(firstBone, lastBone);
            return result;
        }
    }
}
