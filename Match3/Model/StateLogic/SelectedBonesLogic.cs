﻿using Match3.Common;
using System;

namespace Match3.Model.StateLogic
{

    /// <summary>
    /// Реализации логики выделения фишек
    /// </summary>
    public class SelectedBonesLogic
    {
        public Bone FirstBone { get; private set; }
        public Bone LastBone { get; private set; }

        public void SelectBone(Bone bone)
        {
            if (FirstBone == null)
                FirstBone = bone;
            else
            {
                if ((FirstBone.Y == bone.Y && Math.Abs(FirstBone.X - bone.X) == 1) || (FirstBone.X == bone.X && Math.Abs(FirstBone.Y - bone.Y) == 1))
                {
                    IsIncompatibleSelect = false;
                }
                else
                    IsIncompatibleSelect = true;
                LastBone = bone;
            }
        }
        
        /// <summary>
        /// True, если выделение фишек допустимо по логике игры
        /// </summary>
        public bool IsTwoBonesSelected { get => (FirstBone != null && LastBone != null); }

        /// <summary>
        /// Флаг устанавливается если выбранные фишки не совместимы по координатам (лежат либо не на одной линии
        /// или не рядом)
        /// </summary>
        public bool IsIncompatibleSelect { get; set; }

        /// <summary>
        /// Отмена выделения
        /// </summary>
        public void ResetSelect()
        {
            FirstBone = null;
            LastBone = null;
        }
    }
}
