﻿using Match3.Common;
using Match3.Model.StateAnalyzers;
using System.Collections.Generic;
using System.Windows;

namespace Match3.Model.StateLogic
{
/// <summary>
///  Логика зависших элементов и дополнения вакнтных мест
/// </summary>
    class ShiftBonesLogic
    {
        private readonly GameMatrix _gameMatrix;
        private readonly VoidAnalyzer _voidAnalizer;
        private readonly FreeSpaceAnalyzer _freeSpaceAnalizer;
        private readonly LinesAnalyzer _lineAnalizer;

        public ShiftBonesLogic(GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
            _voidAnalizer = new VoidAnalyzer(_gameMatrix);
            _freeSpaceAnalizer = new FreeSpaceAnalyzer(_gameMatrix);
            _lineAnalizer = new LinesAnalyzer(_gameMatrix);

        }

        /// <summary>
        /// Сдвигает группу подвисших элементов
        /// </summary>
        /// <param name="depth">Глубина на которую был произведен сдвиг</param>
        /// <returns>Группа элементов или null если элементов больше нет</returns>
        public List<Bone> Shift (out int depth)
        {           
            
            var shiftingBones = _voidAnalizer.Analyze(out depth);
            if (shiftingBones != null)
                _gameMatrix.ShiftBones(shiftingBones, depth);
            return shiftingBones;
        }

        /// <summary>
        /// Дополняет матрицу новыми элементами
        /// </summary>
        /// <returns>Возвращает список добавленные элементов</returns>
        public List<Bone> СomplementMatrix ()
        {
            
            var result = _freeSpaceAnalizer.Analyze();
            List<Bone> bones = new List<Bone>();
            foreach (Point point in result)
            {
                Bone bone = _gameMatrix.CreateNewBoneAt((int)point.X, (int)point.Y);
                bones.Add(bone);
            }
            return result.Count == 0? null: bones;
        }

        /// <summary>
        /// Сканирует матрицу на наличие одинаковых элементов 3 и более подряд
        /// </summary>
        /// <returns>Возвращает найденные елементы иначе null</returns>
        public List<Bone> CheckMatrixForСoincidence ()
        {            
            var result = _lineAnalizer.FullAnalize();
            return result == true ? _lineAnalizer.SelectedBones : null;
        }

    }
}
