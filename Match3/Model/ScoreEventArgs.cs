﻿using System;


namespace Match3.Model
{
    public class ScoreEventArgs
    {
        public ScoreEventArgs(int score)
        {
            Score = score;
        }

        public int Score { get; set; }
    }
}
