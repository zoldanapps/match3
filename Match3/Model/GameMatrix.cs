﻿using Common.Enums;
using Match3.Common;
using Match3.Common.Helpers;
using System.Collections.Generic;
using System.Linq;


namespace Match3.Model
{
    /// <summary>
    /// Абстракция матрицы элементов и ее логики
    /// </summary>
    public class GameMatrix
    {
        private Bone[,] _bonesMatrix = new Bone[8, 8];
        private int _lastId = 1;
        public List<Bone> Bones
        {
            get => _bonesMatrix.Cast<Bone>().ToList();           
        }

        /// <summary>
        /// Создает новую матрицу
        /// </summary>
        public void InitBonesMatrix()
        {
            _lastId = 1;
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                {
                    Bone bone = CreateRandomBone();
                    bone.Bonus = Bonuses.None;
                    bone.X = x + 1;
                    bone.Y = y + 1;
                    SetUniqColor(bone);
                    _bonesMatrix[x, y] = bone;
                }
            }         
        }

        /// <summary>
        /// Устанавливает цвет фишки так что-бы он был уникален среди её соседей
        /// </summary>
        /// <param name="bone"></param>
        private void SetUniqColor(Bone bone)
        {
            int x = bone.X - 1;
            int y = bone.Y - 1;

            if (x == 0 && y == 0)
                bone.Color = ColorHelper.GetRandomColor();
            else
            {
                // Есть сосед слева
                if (y == 0 && x > 0)
                {
                    bone.Color = ColorHelper.FindRandomColor(_bonesMatrix[x - 1, y].Color);
                    return;
                }
                // Есть сосед сверху
                if (y > 0 && x == 0)
                {
                    bone.Color = ColorHelper.FindRandomColor(_bonesMatrix[x, y - 1].Color);
                    return;
                }
                // Есть сосед сверху и слева
                if (y > 0 && x > 0)
                {
                    bone.Color = ColorHelper.FindRandomColor(_bonesMatrix[x, y - 1].Color, _bonesMatrix[x - 1, y].Color);
                    return;
                }              
            }
        }

        public Bone GetBoneById(int id)
        {
            for (int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                    if (_bonesMatrix[x, y]?.Id == id)
                        return _bonesMatrix[x, y];
            }
            return null;
        }

        public Bone CreateRandomBone()
        {
            BoneColors rndColor = ColorHelper.GetRandomColor();
            return new Bone() { Color = rndColor, Id = _lastId++ };
        }

        public void UpdateBones(List<Bone> bones)
        {
            foreach(Bone bone in bones)
            {
                int xIndex = bone.X - 1;
                int yIndex = bone.Y - 1;
                _bonesMatrix[xIndex, yIndex] = bone;               
            }
        }

        public Bone CreateNewBoneAt(int x, int y )
        {
            Bone bone = CreateRandomBone();
            bone.X = x + 1;
            bone.Y = y + 1;            
            SetUniqColor(bone);
            _bonesMatrix[x, y] = bone;
            return bone;
        }

        public void ChangeBones(Bone firstBone, Bone lastBone)
        {
            
         
            int firstXIndex = firstBone.X - 1;
            int firstYIndex = firstBone.Y - 1;
            int lastXIndex = lastBone.X - 1;
            int lastYIndex = lastBone.Y - 1;

            int firsX = firstBone.X;
            int firsY = firstBone.Y;
            int lastX = lastBone.X;
            int lastY = lastBone.Y;
            Bone temp = _bonesMatrix[firstXIndex, firstYIndex];            
            _bonesMatrix[firstXIndex, firstYIndex] = _bonesMatrix[lastXIndex, lastYIndex];
            _bonesMatrix[firstXIndex, firstYIndex].X = firsX;
            _bonesMatrix[firstXIndex, firstYIndex].Y = firsY;

            
            _bonesMatrix[lastXIndex, lastYIndex] = temp;
            _bonesMatrix[lastXIndex, lastYIndex].X = lastX;
            _bonesMatrix[lastXIndex, lastYIndex].Y = lastY;            
        }

        public void RemoveBones(List<Bone> bones)
        {
            foreach (Bone bone in bones)
            {
                int xIndex = bone.X - 1;
                int yIndex = bone.Y - 1;
                _bonesMatrix[xIndex, yIndex] = null;
            }
        }

        public void ShiftBones(List<Bone> bones, int depth)
        {
            foreach (Bone bone in bones)
            {
                int xIndex = bone.X - 1;
                int yIndex = bone.Y - 1;
                int yDst = yIndex + depth;
                _bonesMatrix[xIndex, yDst] = _bonesMatrix[xIndex, yIndex];
                _bonesMatrix[xIndex, yIndex] = null;
                _bonesMatrix[xIndex, yDst].Y += depth;
            }
        }

        public Bone[,] BoneMatrix
        {
            get => _bonesMatrix;
        }

        public bool IsBoneExist(int x, int y)
        {
            return !(x > 7 || y > 7 || x < 0 || y < 0) && _bonesMatrix[x, y] != null;
        }
    }
}
