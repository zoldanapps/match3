﻿using Match3.Common;
using System.Collections.Generic;


namespace Match3.Model.StateAnalyzers
{
    /// <summary>
    /// Анализатор висячих элементов
    /// </summary>
    public class VoidAnalyzer
    {
        private readonly GameMatrix _gameMatrix;

        public VoidAnalyzer(GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
        }

        /// <summary>
        /// Сканирует матрицу до первого подвисшего блока. Возвращает коллекцию зависших фишек и глубину
        /// Если блоков нет, то возвращает null
        /// </summary>
        /// <param name="depth">Глубина пустот под фишками</param>
        /// <returns></returns>
        public List<Bone> Analyze(out int depth)
        {
            depth = -1;            
            for (int y = 0; y < 8; y++ )
            {
                for (int x = 0; x < 8; x++)
                {
                    if (_gameMatrix.BoneMatrix[x, y] == null && y != 0)
                    {
                        depth = GetDepth(x, y);
                        var result =  GetFreeBones(x, y);
                        if (result.Count == 0)
                            continue;
                        else
                            return result;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Возвращает глубину (высоту) зависания блока
        /// </summary>
        /// <param name="x0"></param>
        /// <param name="y0"></param>
        /// <returns></returns>
        private int GetDepth(int x0, int y0)
        {
            int y;
            for (y = y0; y < 8; y++)
            {
                if (_gameMatrix.BoneMatrix[x0, y] != null)
                    break;
                    
            }
            return y - y0;
        }

        private List<Bone> GetFreeBones(int x0, int y0)
        {
            List<Bone> result = new List<Bone>();
            for (int y = y0 - 1; y >= 0; y--)
            {
                if (_gameMatrix.BoneMatrix[x0, y] != null)
                    result.Add(_gameMatrix.BoneMatrix[x0, y]);
                else
                    break;
            }
            return result;
        }
    }
}
