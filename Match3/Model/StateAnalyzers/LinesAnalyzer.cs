﻿using Common.Enums;
using Match3.Common;
using System.Collections.Generic;


namespace Match3.Model.StateAnalyzers
{
    /// <summary>
    /// Анализ мартрици на линии заполненыее одинаковыми элементами и расчитывает бонусные элементы
    /// </summary>

    public class LinesAnalyzer
    {
        private readonly GameMatrix _gameMatrix;

        /// <summary>
        /// Результат совпадения
        /// </summary>
        public List<Bone> SelectedBones { get; set; }

        /// <summary>
        /// Бонусы за совпадения
        /// </summary>
        public List<Bone> BonusBones { get; set; }

        public LinesAnalyzer(GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
            SelectedBones = new List<Bone>();
            BonusBones = new List<Bone>();
        }

        /// <summary>
        /// Анализирует что переданные firstBone или  lastBone элементы входят в группу из одинаковых фишек три и более
        /// </summary>
        /// <param name="firstBone"></param>
        /// <param name="lastBone"></param>
        /// <returns>Возвращает true если анализ положителен</returns>
        public bool Analyze(Bone firstBone, Bone lastBone)
        {

            SelectedBones.Clear();
            BonusBones.Clear();           
            List<Bone> verticalLine;
            List<Bone> horizontalLine;

            Bone boneBonus = (Bone)lastBone.Clone();
            verticalLine = CheckForVertical(lastBone);
           
                SelectedBones.AddRange(verticalLine);


            horizontalLine = CheckForHorizontal(lastBone);           
            SelectedBones.AddRange(horizontalLine);

            if (horizontalLine.Count >= 3 && verticalLine.Count >= 3)
            {
                boneBonus.Bonus = Bonuses.Bomb;
                BonusBones.Add(boneBonus);
            }
            else
            {
                if (horizontalLine.Count > 4)
                {
                    boneBonus.Bonus = Bonuses.Bomb;
                    BonusBones.Add(boneBonus);
                }
                if (horizontalLine.Count == 4)
                {
                    boneBonus.Bonus = Bonuses.HorizontalLine;
                    BonusBones.Add(boneBonus);
                }
                if (verticalLine.Count > 4)
                {
                    boneBonus.Bonus = Bonuses.Bomb;
                    BonusBones.Add(boneBonus);
                }
                if (verticalLine.Count == 4)
                {
                    boneBonus.Bonus = Bonuses.VerticalLine;
                    BonusBones.Add(boneBonus);
                }
            }


                boneBonus = (Bone)firstBone.Clone();
            horizontalLine = CheckForHorizontal(firstBone);
            SelectedBones.AddRange(horizontalLine);           

            verticalLine = CheckForVertical(firstBone);
          
            SelectedBones.AddRange(verticalLine);

         

            if (horizontalLine.Count >= 3 && verticalLine.Count >= 3)
            {
                boneBonus.Bonus = Bonuses.Bomb;
                BonusBones.Add(boneBonus);
            }
            else
            {
                if (horizontalLine.Count > 4)
                {
                    boneBonus.Bonus = Bonuses.Bomb;
                    BonusBones.Add(boneBonus);
                }
                if (horizontalLine.Count == 4)
                {
                    boneBonus.Bonus = Bonuses.HorizontalLine;
                    BonusBones.Add(boneBonus);
                }
                if (verticalLine.Count > 4)
                {
                    boneBonus.Bonus = Bonuses.Bomb;
                    BonusBones.Add(boneBonus);
                }
                if (verticalLine.Count == 4)
                {
                    boneBonus.Bonus = Bonuses.VerticalLine;
                    BonusBones.Add(boneBonus);
                }
            }

            if (SelectedBones.Count >= 3)
                return true;

            SelectedBones.Clear();
            BonusBones.Clear();
            return false;
        }

        /// <summary>
        /// Полностью анализирует матрицу на наличия одинаковых елементов в рядах по три и более
        /// </summary>
        /// <returns>Возвращает true если результат положительный</returns>
        public bool FullAnalize()
        {
            SelectedBones.Clear();            
            var result = CheckHorizontalLines();            
            SelectedBones.AddRange(result);
            result = CheckVerticalLines();
            SelectedBones.AddRange(result);
            return SelectedBones.Count >= 3;           
        }


        /// <summary>
        /// Проверяет матрицу на одинаковые линии элементов по вертикали
        /// </summary>
        /// <returns></returns>
        private List<Bone> CheckHorizontalLines()
        {
            List<Bone> temp = new List<Bone>();
            List<Bone> result = new List<Bone>();
            

            for (int y = 0; y < 8; y++)
            {               
                Bone baseBone = _gameMatrix.BoneMatrix[0, y];
                temp.Clear();
                temp.Add(baseBone);
                for (int x = 1; x < 8; x++)
                {                    
                    if (baseBone.Color == _gameMatrix.BoneMatrix[x, y].Color)
                        temp.Add(_gameMatrix.BoneMatrix[x, y]);
                    else
                    {
                        baseBone = _gameMatrix.BoneMatrix[x, y];
                        if (temp.Count >= 3)                       
                            result.AddRange(temp);                                                
                        temp.Clear();
                        temp.Add(baseBone);
                    }
                }
                if (temp.Count >= 3)
                    result.AddRange(temp);
            }
            return result;
        }

        /// <summary>
        /// Проверяет матрицу на одинаковые линии элементов по вертикали
        /// </summary>
        /// <returns></returns>
        private List<Bone> CheckVerticalLines()
        {
            List<Bone> temp = new List<Bone>();
            List<Bone> result = new List<Bone>();


            for (int x = 0; x < 8; x++)
            {
                Bone baseBone = _gameMatrix.BoneMatrix[x, 0];
                temp.Clear();
                temp.Add(baseBone);
                for (int y = 1; y < 8; y++)
                {
                    if (baseBone.Color == _gameMatrix.BoneMatrix[x, y].Color)
                        temp.Add(_gameMatrix.BoneMatrix[x, y]);
                    else
                    {
                        baseBone = _gameMatrix.BoneMatrix[x, y];
                        if (temp.Count >= 3)
                            result.AddRange(temp);
                        temp.Clear();
                        temp.Add(baseBone);
                    }
                }
                if (temp.Count >= 3)
                    result.AddRange(temp);
            }
            return result;
        }


        /// <summary>
        /// Проверяет, что переданный элемент входит в вертикальную линию одинаковых элементов
        /// </summary>
        /// <param name="selectBone"></param>
        /// <returns></returns>
        private List<Bone> CheckForVertical(Bone selectBone)
        {
            Bone[,] boneMatrix = _gameMatrix.BoneMatrix;
            int x = selectBone.X - 1;
            List<Bone> tempList = new List<Bone>();
            List<Bone> resultList = new List<Bone>();
            tempList.Add(selectBone);

            // Ищим ввеху от элемента
            if (selectBone.Y > 1)
            {
                for (int y = selectBone.Y - 2; y >= 0; y--)
                {
                    if (boneMatrix[x, y]?.Color == selectBone.Color)
                        tempList.Add(boneMatrix[x, y]);
                    else
                        break;
                }
            }
            // Ищем вниз от элемента
            if (selectBone.Y < 8)
            {
                for (int y = selectBone.Y; y < 8; y++)
                {
                    if (boneMatrix[x, y]?.Color == selectBone.Color)
                        tempList.Add(boneMatrix[x, y]);
                    else
                        break;
                }
            }
            if (tempList.Count >= 3)
                resultList.AddRange(tempList);

            return resultList;

        }

        /// <summary>
        /// Проверяет, что переданный элемент входит в горизонтальную линию одинаковых элементов
        /// </summary>
        /// <param name="selectBone"></param>
        /// <returns></returns>
        private List<Bone> CheckForHorizontal(Bone selectBone)
        {
            Bone[,] boneMatrix = _gameMatrix.BoneMatrix;
            int y = selectBone.Y - 1;

            List<Bone> tempList = new List<Bone>();
            List<Bone> resultList = new List<Bone>();
            tempList.Add(selectBone);

            // Идем вправо от элемента
            if (selectBone.X < 8)
            {
                for (int x = selectBone.X; x < 8; x++)
                {
                    if (boneMatrix[x, y]?.Color == selectBone.Color)
                        tempList.Add(boneMatrix[x, y]);
                    else
                        break;
                }
            }

            // Идем влево от элемента
            if (selectBone.X > 1)
            {
                for (int x = selectBone.X - 2; x >= 0; x--)
                {
                    if (boneMatrix[x, y]?.Color == selectBone.Color)
                        tempList.Add(boneMatrix[x, y]);
                    else
                        break;
                }
                if (tempList.Count >= 3)
                    resultList.AddRange(tempList);
            }
            return resultList;
        }      
    }
}
