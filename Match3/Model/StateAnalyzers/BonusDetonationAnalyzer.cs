﻿using Common.Enums;
using Match3.Common;
using System.Collections.Generic;


namespace Match3.Model.StateAnalyzers
{
    /// <summary>
    /// Делает анализ разрушительной способности бонусного элемента
    /// </summary>
    public class BonusDetonationAnalyzer
    {
        private readonly GameMatrix _gameMatrix;

        public BonusDetonationAnalyzer (GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
        }

        /// <summary>
        /// Производит анализ и возвращает элементы которые попадают под разрушения бонусным элементом
        /// </summary>
        /// <param name="bonusBone"></param>
        /// <returns></returns>
        public List<Bone> Analyze(Bone bonusBone)
        {
            if (bonusBone.Bonus == Bonuses.HorizontalLine)
                return GetHorizontalNeighbors(bonusBone);
            if (bonusBone.Bonus == Bonuses.VerticalLine)
                return GetVerticalNeighbors(bonusBone);
            if (bonusBone.Bonus == Bonuses.Bomb)
                return GetCommonNeighbors(bonusBone);
            return null;
        }

        private List<Bone> GetCommonNeighbors(Bone bonusBone)
        {
            List<Bone> result = new List<Bone>();
            int x = bonusBone.X - 1;
            int y = bonusBone.Y - 1;
            result.Add(bonusBone);
            if (_gameMatrix.IsBoneExist(x, y + 1))
                result.Add(_gameMatrix.BoneMatrix[x, y + 1]);
            if (_gameMatrix.IsBoneExist(x + 1, y + 1))
                result.Add(_gameMatrix.BoneMatrix[x + 1, y + 1]);
            if (_gameMatrix.IsBoneExist(x + 1, y ))
                result.Add(_gameMatrix.BoneMatrix[x + 1, y]);
            if (_gameMatrix.IsBoneExist(x + 1, y - 1))
                result.Add(_gameMatrix.BoneMatrix[x + 1, y - 1]);
            if (_gameMatrix.IsBoneExist(x, y - 1))
                result.Add(_gameMatrix.BoneMatrix[x, y - 1]);
            if (_gameMatrix.IsBoneExist(x - 1, y - 1))
                result.Add(_gameMatrix.BoneMatrix[x - 1, y - 1]);
            if (_gameMatrix.IsBoneExist(x - 1, y))
                result.Add(_gameMatrix.BoneMatrix[x - 1, y]);
            if (_gameMatrix.IsBoneExist(x - 1, y + 1))
                result.Add(_gameMatrix.BoneMatrix[x - 1, y + 1]);          
            return result;
                
        }       

        private List<Bone> GetHorizontalNeighbors(Bone bone)
        {
            List<Bone> result = new List<Bone>();
            int y = bone.Y - 1;
            for (int x = 0; x < 8; x++)
            {
                if (_gameMatrix.BoneMatrix[x, y] != null)
                    result.Add(_gameMatrix.BoneMatrix[x, y]);
            }
            return result;
        }

        private List<Bone> GetVerticalNeighbors(Bone bone)
        {
            List<Bone> result = new List<Bone>();
            int x = bone.X - 1;
            for (int y = 0; y < 8; y++)
            {
                if (_gameMatrix.BoneMatrix[x, y] != null)
                    result.Add(_gameMatrix.BoneMatrix[x, y]);
            }
            return result;
        }

    }
}
