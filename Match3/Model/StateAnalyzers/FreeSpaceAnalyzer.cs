﻿using System.Collections.Generic;
using System.Windows;

namespace Match3.Model.StateAnalyzers
{
    /// <summary>
    /// Анализирует матрицу на пустые места
    /// </summary>
    public class FreeSpaceAnalyzer
    {
        private readonly GameMatrix _gameMatrix;

        public FreeSpaceAnalyzer (GameMatrix gameMatrix)
        {
            _gameMatrix = gameMatrix;
        }

        /// <summary>
        /// Возвращает набор точек пустых мест матрици
        /// </summary>
        /// <returns></returns>
        public List<Point> Analyze()
        {
            List<Point> result = new List<Point>();
            for (int y = 0; y < 8; y++)
            {
                for (int x = 0; x < 8; x++)
                    if (_gameMatrix.BoneMatrix[x, y] == null)
                        result.Add(new Point(x, y));
            }
            return result;
        }
    }
}
