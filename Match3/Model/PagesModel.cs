﻿using Match3.Common.Enums;

namespace Match3.Model
{
    /// <summary>
    /// Модель управления страницами представления
    /// </summary>
    public class PagesModel: MvvmBase
    {       
        private ActivePages _selectPage;
        public ActivePages SelectPage 
        {
            get { return _selectPage; }
            set
            {
                if (_selectPage != value)
                {
                    _selectPage = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
