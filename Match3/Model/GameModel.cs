﻿using Common.Enums;
using Match3.Common;
using Match3.Common.Enums;
using Match3.Model.StateAnalyzers;
using System;
using System.Collections.Generic;
using Match3.Model.StateLogic;
using System.Timers;

namespace Match3.Model
{
    /// <summary>
    /// Игровая модель
    /// </summary>
    public class GameModel : MvvmBase
    {
        public event EventHandler<ModelActionEventArgs> NewGameStarted;
        public event EventHandler<ModelActionEventArgs> BoneSelected;
        public event EventHandler<ModelActionEventArgs> BonesChenged;
        public event EventHandler<ModelActionEventArgs> SelectReseted;
        public event EventHandler<ModelActionEventArgs> BonesDestroyed;
        public event EventHandler<ModelActionEventArgs> BonusBoneDestroyed;
        public event EventHandler<ModelActionEventArgs> NewBonesCreated;
        public event EventHandler<ModelActionEventArgs> BonesSheeted;       
        public event EventHandler<ScoreEventArgs> GameOver;       

        private readonly GameMatrix _gameMatrix = new GameMatrix();
        private List<Bone> _selectBones;
        private ModelStates _state = ModelStates.None;

        private List<Bone> _bonusBones;
        private List<Bone> _destroingBonusBones;


        private SelectedBonesLogic _selectedBonesLogic;
        private ChangeBonesLogic _changeBonesLogic;
        private ShiftBonesLogic _shiftBonesLogic;

        private readonly Timer _gameTimer = new Timer(1000);

        /// <summary>
        /// Продолжительность игры в сек
        /// </summary>
        private const int GameElapse = 60;
        /// <summary>
        /// Число очков за один удаленный элемент
        /// </summary>
        private const int DestroyBoneScore = 10;        
        /// <summary>
        /// Число очков за один удаленный элемент в случае срабатывания бунусного элемента
        /// </summary>
        private const int DestroyBonusScore = 40;
        private int _secondCount;
        private int _scoreCount;


        public int SecondCount
        {
            get { return _secondCount; }
            set
            {
                _secondCount = value;
                OnPropertyChanged();
            }
        }

        public int ScoreCount
        {
            get { return _scoreCount; }
            set
            {
                _scoreCount = value;
                OnPropertyChanged();
            }
        }

        public GameModel()
        {
            _gameTimer.Elapsed += GameTimer_Elapsed;
        }

        private void GameTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            SecondCount++;
            if (SecondCount == GameElapse)
            {
                GameOver?.Invoke(this, new ScoreEventArgs(_scoreCount));
                _gameTimer.Stop();
            }           
        }

        public void StartNewGame()
        {
            _gameMatrix.InitBonesMatrix();
            _state = ModelStates.NewGame;
            _selectedBonesLogic = new SelectedBonesLogic();
            _changeBonesLogic = new ChangeBonesLogic(_gameMatrix);
            _shiftBonesLogic = new ShiftBonesLogic(_gameMatrix);
            NewGameStarted?.Invoke(this, new ModelActionEventArgs(_gameMatrix.Bones));
            ScoreCount = 0;
            SecondCount = 0;
            _gameTimer.Start();
        }

        public void SelectBone(int boneId)
        {

            if (_state == ModelStates.SelectBone)
            {
                var bone = _gameMatrix.GetBoneById(boneId);
                _selectedBonesLogic.SelectBone(bone);
                if (_selectedBonesLogic.IsTwoBonesSelected)
                {
                    if (_selectedBonesLogic.IsIncompatibleSelect)
                    {
                        SelectReseted?.Invoke(this, new ModelActionEventArgs(new List<Bone> { _selectedBonesLogic.FirstBone }));
                        _selectedBonesLogic.ResetSelect();
                        _state = ModelStates.SelectBone;
                    }
                    else
                    {
                        _state = ModelStates.TryChangeBones;
                        BonesChenged?.Invoke(this, new ModelActionEventArgs(new List<Bone> { _selectedBonesLogic.FirstBone, _selectedBonesLogic.LastBone }));                        
                    }
                }
                else
                {
                    BoneSelected?.Invoke(this, new ModelActionEventArgs(new List<Bone>() { _selectedBonesLogic.FirstBone }));
                    _state = ModelStates.SelectBone;
                }
            }
        }

        private void TryChangeBone()
        {
            // Дает игровую комбинацию
            if (_changeBonesLogic.TryChangeBone(_selectedBonesLogic.FirstBone, _selectedBonesLogic.LastBone))
            {
                _selectBones = _changeBonesLogic.SelectedBones;
                _bonusBones = _changeBonesLogic.BonusBones;
                _state = ModelStates.DestroyBones;
                DestroyBones();
                _selectedBonesLogic.ResetSelect();
            }
            else
            {
                // Откат
                BonesChenged?.Invoke(this, new ModelActionEventArgs(new List<Bone> { _selectedBonesLogic.FirstBone, _selectedBonesLogic.LastBone }));
                _selectedBonesLogic.ResetSelect();
                _state = ModelStates.SelectBone;
            }
        }       

        private void DestroyBones()
        {
            // Бонусные элементы среди элементов для разрушения
            _destroingBonusBones = _selectBones.FindAll((b) => b.Bonus != Bonuses.None);
            // Если бонусных элементов нет среди разрушаемых, то просто разрушаем выпавшие фишки
            if (_destroingBonusBones.Count == 0)
            {
                _gameMatrix.RemoveBones(_selectBones);
                ScoreCount += DestroyBoneScore * _selectBones.Count;
                BonesDestroyed?.Invoke(this, new ModelActionEventArgs(_selectBones));

                // Если есть бонусы за разрушение (4 и более более подряд элементов), то создаем бонусные фишки
                if (_bonusBones.Count > 0)
                    _state = ModelStates.CreateBonusBones;
                // Если бонусных елементов нет то сдвигаем фишки вниз
                else
                    _state = ModelStates.ShiftBones;
            }
            // Если среди взрываемых есть фишки с бонусами
            else
            {
                //Удаляем бонусные элементы из общей группы
                foreach (Bone bonusBone in _destroingBonusBones)
                    _selectBones.Remove(bonusBone);
                _gameMatrix.RemoveBones(_selectBones);
                // Сначало уничтожаем обычные элементы, потом взрываем бонусные
                BonesDestroyed?.Invoke(this, new ModelActionEventArgs(_selectBones));
                _state = ModelStates.DestroyBonusBones;
            }
        }

        private void DestroyBonusBones()
        {
            if (_destroingBonusBones?.Count > 0)
            {
                BonusDetonationAnalyzer detonationAnalizer = new BonusDetonationAnalyzer(_gameMatrix); 
                var destroingBones = detonationAnalizer.Analyze(_destroingBonusBones[0]);
                ScoreCount += DestroyBonusScore * destroingBones.Count;
                BonusBoneDestroyed?.Invoke(this, new ModelActionEventArgs(destroingBones));
                _destroingBonusBones.RemoveAt(0);
                _gameMatrix.RemoveBones(destroingBones);
                if (_destroingBonusBones.Count == 0)
                {            
                    _state = ModelStates.ShiftBones;
                }
            }
        }

        private void CreateBonusBones()
        {
            _gameMatrix.UpdateBones(_bonusBones);
            NewBonesCreated?.Invoke(this, new ModelActionEventArgs(_bonusBones));
            _state = ModelStates.ShiftBones;
        }

        private void ShiftBones()
        {            
            var shiftingBones = _shiftBonesLogic.Shift(out int depth);
            if (shiftingBones != null)
                BonesSheeted?.Invoke(this, new ModelActionEventArgs(shiftingBones) { Depth = depth });
            else
            {
                var newBones = _shiftBonesLogic.СomplementMatrix();
                NewBonesCreated?.Invoke(this, new ModelActionEventArgs(newBones));
                _state = ModelStates.RestoreMatrix;                
            }
        }

        /// <summary>
        /// Восстаовление матрицы до нормального состояния.
        /// После матча и взыва элементов остаются пустые места.
        /// Этот метод анализирует состояние, сдвигает подвисшие  элементы вниз, создает новые
        /// </summary>
        private void RestoreMatrix()
        {
            var result = _shiftBonesLogic.CheckMatrixForСoincidence();
            if (result != null)
            {
                ScoreCount += result.Count * DestroyBoneScore;
                BonesDestroyed(this, new ModelActionEventArgs(result));
                _gameMatrix.RemoveBones(result);
                _state = ModelStates.ShiftBones;
            }
            else
                _state = ModelStates.SelectBone;
        }

        /// <summary>
        /// Обновление состояния.
        /// Метод дергается после завершения предыдущей анимации
        /// </summary>
        public void UpdateState()
        {  
            switch(_state)
            {
                case ModelStates.NewGame:
                    _state = ModelStates.SelectBone;
                    break;
                case ModelStates.DestroyBones:
                    DestroyBones();
                    break;
                case ModelStates.CreateBonusBones:
                    CreateBonusBones();
                    break;
                case ModelStates.ShiftBones:
                    ShiftBones();
                    break;
                case ModelStates.RestoreMatrix:
                    RestoreMatrix();
                    break;
                case ModelStates.DestroyBonusBones:
                    DestroyBonusBones();
                    break;
                case ModelStates.TryChangeBones:
                    TryChangeBone();
                    break;
            }           
        }      
    }
}
