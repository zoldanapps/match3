﻿using System.ComponentModel;
using System.Runtime.CompilerServices;


namespace Match3.Model
{
    public class MvvmBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
    }
}
