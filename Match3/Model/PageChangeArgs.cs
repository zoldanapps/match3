﻿using Match3.Common.Enums;
using System;


namespace Match3.Model
{
    public class PageChangeArgs: EventArgs
    { 
        public ActivePages SelectedPage { get; }
        public PageChangeArgs (ActivePages selectPage)
        {
            SelectedPage = selectPage;
        }
    }
}
