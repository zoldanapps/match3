﻿using Match3.ViewModel;
using System.Windows.Controls;


namespace Match3.View
{
    /// <summary>
    /// Логика взаимодействия для MainMenuPage.xaml
    /// </summary>
    public partial class MainMenuPage : Page
    {
        public GameViewModel ViewModel { get; set; }
        public MainMenuPage(GameViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            DataContext = ViewModel;
        }        
    }
}
