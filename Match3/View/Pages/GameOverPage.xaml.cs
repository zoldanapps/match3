﻿using Match3.ViewModel;
using System.Windows;
using System.Windows.Controls;

namespace Match3.View
{
    /// <summary>
    /// Логика взаимодействия для GameOverPage.xaml
    /// </summary>
    public partial class GameOverPage : Page
    {
        public GameViewModel ViewModel { get; set; }
        public GameOverPage(GameViewModel viewModel)
        {
            ViewModel = viewModel;
            InitializeComponent();
            DataContext = ViewModel;
        }

        private void NewGameButton_Click(object sender, RoutedEventArgs e)
        {
            ViewModel.PlayGameCommand.Execute(null);
        }
    }
}
