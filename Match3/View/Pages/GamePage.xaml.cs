﻿using Match3.Common.Factories;
using Match3.ViewModel;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Match3.View.Pages
{
    /// <summary>
    /// Логика взаимодействия для GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        public GameViewModel ViewModel { get; set; }
        private readonly List<Controls.BoneControl> _uiBones;
        

        public GamePage(GameViewModel viewModel)
        {
            _uiBones = new List<Controls.BoneControl>();
            ViewModel = viewModel;
            ViewModel.StartActionCommand.SetAction((f) => {
                var factory = (AnimationFactory)f;
                factory.Init(GameField, _uiBones);
                IAnimation animation = factory.Create();
                animation.SubscribeOnClick += Animation_SubscribeOnClick;
                animation.UnSubscribeOnClick += Animation_UnSubscribeOnClick;
                animation.AnimationComplited += Animation_AnimationComplited;
                animation.RunAnimation();
            });           
            DataContext = viewModel;
            InitializeComponent();            
        }

        private void Animation_AnimationComplited(object sender, EventArgs e)
        {
            if (ViewModel.ActionComplitedCommand.CanExecute(null))
                ViewModel.ActionComplitedCommand.Execute(null);
        }

        private void Animation_UnSubscribeOnClick(object sender, SubscribeArgs e)
        {
            e.UiBone.Click -= Bone_Click;
        }

        private void Animation_SubscribeOnClick(object sender, SubscribeArgs e)
        {
            e.UiBone.Click += Bone_Click;
        }    

        private void Bone_Click(object sender, RoutedEventArgs e)
        {
            var uiBone = (Controls.BoneControl)sender;
            if (ViewModel.SelectBoneCommand.CanExecute(null))
                ViewModel.SelectBoneCommand.Execute(uiBone.Id);
        }        
    }
}
