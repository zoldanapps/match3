﻿using Match3.Common.Enums;
using Match3.View.Pages;
using Match3.ViewModel;
using System.Windows;


namespace Match3.View
{
    /// <summary>
    /// Логика взаимодействия для GameView.xaml
    /// </summary>
    public partial class GameView : Window
    {
        private readonly GameViewModel _viewModel;
        private ActivePages _curranetPage;

        public ActivePages CurranetPage
        {
            set
            {
                switch (value)
                {
                    case ActivePages.GamePage:
                        GamePage gamePage = new GamePage(_viewModel);
                        Page.Navigate(gamePage);
                        break;
                    case ActivePages.MenuPage:
                        MainMenuPage mainMenuPage = new MainMenuPage(_viewModel);
                        Page.Navigate(mainMenuPage);
                        break;
                    case ActivePages.GameOverPage:
                        GameOverPage gameOverPage = new GameOverPage(_viewModel);
                        Page.Navigate(gameOverPage);
                        break;
                }
                _curranetPage = value;
            }
            get { return _curranetPage; }            
        }
        public GameView(GameViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponent();
            _viewModel.PropertyChanged += ViewModel_PropertyChanged;
        }

        private void ViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "SelectPage")
                if (CurranetPage != _viewModel.SelectPage)
                    CurranetPage = _viewModel.SelectPage;
        }
    }
}
