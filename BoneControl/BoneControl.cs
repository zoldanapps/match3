﻿using Common.Enums;
using System.Windows;
using System.Windows.Controls;

namespace Controls
{
   
    public class BoneControl : Button
    {
        private static readonly DependencyProperty BoneColorProperty;
        private static readonly DependencyProperty BonusProperty;
        static BoneControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BoneControl), new FrameworkPropertyMetadata(typeof(BoneControl)));
            BoneColorProperty = DependencyProperty.Register("BoneColor", typeof(BoneColors), typeof(BoneControl),
                new PropertyMetadata(BoneColors.Yellow));
            BonusProperty = DependencyProperty.Register("Bonus", typeof(Bonuses), typeof(BoneControl),
                new PropertyMetadata(Bonuses.None));
        }

        /// <summary>
        /// Цвет фишки
        /// </summary>
        public BoneColors BoneColor
        {
            get { return (BoneColors)GetValue(BoneColorProperty); }
            set { SetValue(BoneColorProperty, value); }
        }

        /// <summary>
        /// Бонус
        /// </summary>
        public Bonuses Bonus
        {
            get { return (Bonuses)GetValue(BonusProperty); }
            set { SetValue(BonusProperty, value); }
        }

        /// <summary>
        /// Идентификато фишки
        /// </summary>
        public int Id { get; set; }
    }
}
