﻿
namespace Common.Enums
{
    /// <summary>
    /// Бонус
    /// </summary>
    public enum Bonuses
    {
        None,
        HorizontalLine,
        VerticalLine,
        Bomb
    }
}
