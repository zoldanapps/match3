﻿

namespace Common.Enums
{
    /// <summary>
    /// Цвет фишки
    /// </summary>
    public enum BoneColors
    {
        Yellow = 0,
        Orange = 1,
        Green = 2,
        Purple = 3,
        Red = 4
    }
}
